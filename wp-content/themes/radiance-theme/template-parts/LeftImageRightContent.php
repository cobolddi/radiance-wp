<section class="left_image_right_content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6" data-aos="fade-out" data-aos-easing="linear" data-aos-duration="400">
              <div class="imgWrap">
                  <img src="assets/img/ecosystem.png" alt="gift_image">
              </div>
            </div>
            <div class="col-md-6">
                <div class="contentWrap mr-5" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
                    <h2 class="LiteOrangeBorderBottom GreyText">The Radiance Ecosystem</h2>
                    <p class="GreyText"><strong>Radiance Renewables was incorporated in 2018</strong> as a private equity owned developer of competitive renewable energy solutions for commercial, industrial and residential customers, enabling them to achieve their sustainability goals.</p>
                    <p class="GreyText"><strong>Radiance Renewables is a 100% subsidiary of the Green Growth Equity Fund (GGEF)</strong>, an Alternative Investment Fund managed by EverSource Capital, with cornerstone investments from India’s National Investment and Infrastructure Fund (NIIF) and the Department for International Development (DFID) of the UK government.</p>
                    <a href="#" class="OrangeYellowButton"><span>Read more</span></a>
                </div>
            </div>

        </div>
    </div>
</section>