<section class="Section ApplyHereSection">
	<div class="container">
		<div class="Topheading">
			<h2 class="WhiteBorderBottom">Apply Here</h2>
		</div>
		<div class="ApplyFormBlock" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
			<?php echo do_shortcode('[contact-form-7 id="248" title="Apply Now Form"]'); ?>
		</div>
	</div>
</section>