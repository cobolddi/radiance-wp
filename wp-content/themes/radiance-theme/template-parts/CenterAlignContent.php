<?php

$CenterContent = get_field('only_content_section');

?>

<section class="Section CenterAlignContent">
	<div class="container">
		<div class="CenterContent XtraSmallContainer" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
			<h2 class="LiteOrangeBorderBottom"><?php echo $CenterContent['heading']; ?></h2>
			<?php echo $CenterContent['content']; ?>			
		</div>
	</div>
</section>