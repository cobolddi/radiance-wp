<?php 

$TestimonialSlider = get_field('testimonial_slider');

?>

<section class="Section LiteOrangeSection TestimonialSlider">
	<div class="container">
		<div class="TopHeading">
			<h2 class="LiteOrangeBorderBottom"><?php echo $TestimonialSlider['heading']; ?></h2>
		</div>
		<div class="Slider">
			<?php if( have_rows('testimonial_slider') ): while ( have_rows('testimonial_slider') ) : the_row(); ?>
				<div class="testimonialslider">
					<?php if( have_rows('testimonial_slides') ): while ( have_rows('testimonial_slides') ) : the_row(); ?>
					  	<div class="TestimonialBlock">
					  		<p class="testimonial"><?php echo get_sub_field('testimony'); ?></p>
					  		<h4><?php echo get_sub_field('author'); ?></h4>
					  		<p class="designation"><?php echo get_sub_field('designation'); ?> <br><span><?php echo get_sub_field('company'); ?></span></p>
					  	</div>
					<?php endwhile; endif; ?>
				</div>
			<?php endwhile; endif; ?>
		</div>
	</div>
</section>