<?php 

$TeamBlock = get_field('bod_cards');
$ManagementBlock = get_field('management_cards');

?>

<section class="Section LiteOrangeSection TeamCardsBlock">
	<div class="container">
		<div class="TeamCards">		
		<?php if( have_rows('bod_cards') ): while ( have_rows('bod_cards') ) : the_row(); ?>
			<div class="row mb5">				
				<div class="col-12">
					<div class="TopHeading">
						<h2 class="LiteOrangeBorderBottom"><?php echo $TeamBlock['heading']; ?></h2>
					</div>
				</div>
				<?php if( have_rows('cards') ): while ( have_rows('cards') ) : the_row(); ?>
					<div class="col-12 col-md-4">						
						<div class="Cards">
							<div class="TopImg">
								<img src="<?php echo get_sub_field('card_image'); ?>" alt="">
								<div class="OnHoverBox">
									<a href="#<?php echo get_sub_field('popup_id'); ?>" class="btn open-popup-link">Read More</a>
								</div>
							</div>
							<div class="TitleHead">
								<h4><?php echo get_sub_field('card_title'); ?></h4>
								<p><?php echo get_sub_field('card_designation'); ?></p>
							</div>
						</div>
						<div id="<?php echo get_sub_field('popup_id'); ?>" class="showmessage white-popup mfp-hide">
							<h4><?php echo get_sub_field('popup_heading'); ?></h4>
							<p><?php echo get_sub_field('popup_designation'); ?></p>
							<?php echo get_sub_field('circle_bullets'); ?>
							<?php echo get_sub_field('tri_bullets'); ?>
						</div>
					</div>
				<?php endwhile; endif; ?>
			</div>
			<?php endwhile; endif; ?>


			<?php if( have_rows('management_cards') ): while ( have_rows('management_cards') ) : the_row(); ?>
				<div class="row">					
						<div class="col-12">
							<div class="TopHeading">
								<h2 class="LiteOrangeBorderBottom"><?php echo $ManagementBlock['heading']; ?></h2>
							</div>
						</div>	
					<?php if( have_rows('cards') ): while ( have_rows('cards') ) : the_row(); ?>				
						<div class="col-12 col-md-4">						
							<div class="Cards">
								<div class="TopImg">
									<img src="<?php echo get_sub_field('card_image'); ?>" alt="">
								</div>
								<div class="TitleHead">
									<h4><?php echo get_sub_field('card_title'); ?></h4>
									<p><?php echo get_sub_field('card_designation'); ?></p>
								</div>
							</div>
						</div>
					<?php endwhile; endif; ?>
				</div>
			<?php endwhile; endif; ?>
		</div>
	</div>
</section>