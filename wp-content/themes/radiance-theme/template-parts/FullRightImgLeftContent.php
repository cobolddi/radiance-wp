<?php 

$FullRightImgLeftContent = get_field('full_right_img_left_content'); 

?>


<section class="left_image_right_content LiteOrangeSection pb7">
	<div class="row">
		<div class="col-md-6 IpadRemoved" data-aos="fade-out" data-aos-easing="linear" data-aos-duration="400">
          <div class="imgWrap">
              <img src="<?php echo $FullRightImgLeftContent['right_img']['url']; ?>" alt="">
          </div>
        </div>

        <div class="col-md-6 IpadLeftView">
            <div class="contentWrap text-left-md">
                <div class="LeftIconRightContent">
                    <h2 class="LiteOrangeBorderBottom GreyText" data-aos="fade-out" data-aos-easing="linear" data-aos-duration="400">Our Vision</h2>
                    <?php if( have_rows('full_right_img_left_content') ): while ( have_rows('full_right_img_left_content') ) : the_row(); ?>
                    	<ul data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
                            <?php if( have_rows('icon_with_right_content') ): while ( have_rows('icon_with_right_content') ) : the_row(); ?>
                        		<li>
                        			<div class="Icon">
                        				<img src="<?php echo get_sub_field('icon'); ?>" alt="">
                        			</div>
                        			<div class="Content">
                        				<h2><?php echo get_sub_field('heading'); ?></h2>
                        				<p><?php echo get_sub_field('content'); ?></p>
                        			</div>
                        		</li>
                            <?php endwhile; endif; ?>
                    	</ul>
                    <?php endwhile; endif; ?>
                </div>
            </div>
        </div>

        <div class="col-md-6 IpadDesktop" data-aos="fade-out" data-aos-easing="linear" data-aos-duration="400">
          <div class="imgWrap">
              <img src="<?php echo $FullRightImgLeftContent['right_img']['url']; ?>" alt="">
          </div>
        </div>
	</div>
</section>