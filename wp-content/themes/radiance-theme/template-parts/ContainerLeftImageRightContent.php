<?php 

$leftrightcontent = get_field('container_left_image_right_content'); 

?>


<section class="Section ContainerLeftImageRightContent">
	<div class="container">
		<div class="Topheading" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
			<h2 class="LiteOrangeBorderBottom"><?php echo $leftrightcontent['heading']; ?></h2>
		</div>
		<div class="LeftImgRightContent" style="background: url(<?php echo $leftrightcontent['left_image']['url']; ?>) no-repeat;">
			<div class="LeftImg">
				<img src="<?php echo $leftrightcontent['left_image']['url']; ?>" alt="">
			</div>
			<div class="RightContent"  data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
				<?php echo $leftrightcontent['right_content']; ?>
			</div>
		</div>
	</div>
</section>