<?php  
    
    $solar = get_field('solar_basics_faqs');
    $solarRight = get_field('solar_right_solution');
    $radiancefaq = get_field('radiance_solution_faqs');

?>


<section class="Section AccordionSection">
    <div class="container">
        <div class="wrap" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
            <h2>
                <span><img src="<?php echo $solar['icon']; ?>" alt=""></span>
                <?php echo $solar['heading']; ?>
            </h2>
            <?php if( have_rows('solar_basics_faqs') ): while ( have_rows('solar_basics_faqs') ) : the_row(); ?>
                <ul class="accordion">
                    <?php if( have_rows('accordians') ): while ( have_rows('accordians') ) : the_row(); ?>
                        <li class="accordion__item">
                            <a class="accordion__title" href=""><?php echo get_sub_field('title'); ?>
                                <div class="bmenu x7">
                                    <span class="btop"></span><span class="bbot"></span>
                                </div>
                            </a>
                            <div class="accordion__content">
                                <?php echo get_sub_field('content'); ?>
                            </div>
                        </li>
                    <?php endwhile; endif; ?>
                </ul>
            <?php endwhile; endif; ?>
        </div>

        <div class="wrap" id="rightsolution" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
            <h2>
                <span><img src="<?php echo $solarRight['icon']; ?>" alt=""></span>
                <?php echo $solarRight['heading']; ?>
            </h2>
            <?php if( have_rows('solar_right_solution') ): while ( have_rows('solar_right_solution') ) : the_row(); ?>
                <ul class="accordion">
                    <?php if( have_rows('accordians') ): while ( have_rows('accordians') ) : the_row(); ?>
                        <li class="accordion__item">
                            <a class="accordion__title" href=""><?php echo get_sub_field('title'); ?>
                                <div class="bmenu x7">
                                    <span class="btop"></span><span class="bbot"></span>
                                </div>
                            </a>
                            <div class="accordion__content">
                                <?php echo get_sub_field('content'); ?>
                            </div>
                        </li>
                    <?php endwhile; endif; ?>
                </ul>
            <?php endwhile; endif; ?>
        </div>
        <div class="wrap" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="500" id="PowerSupply">
            <h2>
                <span><img src="<?php echo $radiancefaq['icon']; ?>" alt=""></span>
                <?php echo $radiancefaq['heading']; ?>
            </h2>
            <?php if( have_rows('radiance_solution_faqs') ): while ( have_rows('radiance_solution_faqs') ) : the_row(); ?>
                <ul class="accordion">
                    <?php if( have_rows('accordians') ): while ( have_rows('accordians') ) : the_row(); ?>
                        <li class="accordion__item">
                            <a class="accordion__title" href=""><?php echo get_sub_field('title'); ?>
                                <div class="bmenu x7">
                                    <span class="btop"></span><span class="bbot"></span>
                                </div>
                            </a>
                            <div class="accordion__content">
                                <?php echo get_sub_field('content'); ?>
                            </div>
                        </li>
                    <?php endwhile; endif; ?>
                </ul>
            <?php endwhile; endif; ?>
        </div>
    </div>
</section>