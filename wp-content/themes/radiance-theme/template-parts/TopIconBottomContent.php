<section class="Section WhoWeAre TopIconBottomContentSection">
	<div class="container">
		<div class="TopIconBottomHeading">
			<?php if( have_rows('top_icon_bottom_content') ): while ( have_rows('top_icon_bottom_content') ) : the_row(); ?>
				<div class="row">
					<?php if( have_rows('icon_with_content') ): while ( have_rows('icon_with_content') ) : the_row(); ?>
						<div class="col-12 col-md-3" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
							<div class="IconWithHeading">
								<div class="IconBox">
									<img src="<?php echo get_sub_field('icon'); ?>" alt="">
								</div>
								<h4><?php echo get_sub_field('heading'); ?></h4>
								<p><?php echo get_sub_field('content'); ?></p>
							</div>
						</div>
					<?php endwhile; endif; ?>
				</div>
			<?php endwhile; endif; ?>
		</div>	
	</div>
</section>