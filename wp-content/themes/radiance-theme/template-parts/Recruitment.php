<?php 

$recruitmentBlock = get_field('recruitment_section');

?>


<section class="Section RecruitmentSection">
	<div class="container SmallContainer">
		<div class="TopHeading">
			<h2 class="LiteOrangeBorderBottom"><?php echo $recruitmentBlock['heading']; ?></h2>
			<p><?php echo $recruitmentBlock['subheading']; ?></p>
			<a href="<?php echo $recruitmentBlock['button_link']; ?>" class="OrangeYellowButton" target="_blank"><span><?php echo $recruitmentBlock['button_text']; ?></span></a>
		</div>
		<?php if( have_rows('recruitment_section') ): while ( have_rows('recruitment_section') ) : the_row(); ?>
			<div class="ThreeCardsBlock">
				<?php if( have_rows('three_cards_block') ): while ( have_rows('three_cards_block') ) : the_row(); ?>
					<div class="LeftIconRightContentBox" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
						<div class="LeftIcon">
							<img src="<?php echo get_sub_field('image'); ?>" alt="">
							<h4><?php echo get_sub_field('heading'); ?></h4>
						</div>
						<div class="RightContentBox">
							<?php echo get_sub_field('content'); ?>
						</div>
					</div>
				<?php endwhile; endif; ?>
			</div>
		<?php endwhile; endif; ?>
	</div>
</section>