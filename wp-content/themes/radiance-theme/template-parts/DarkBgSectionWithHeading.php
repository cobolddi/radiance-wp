<section class="Section DarkBgSectionWithHeading darkBgSection">
	<div class="container">
		<div class="TopHeading" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="200">
			<h2 class="LiteOrangeBorderBottom">We provide an end-to-end partnership solution in 3 phases.</h2>
			<h4>The Radiance Renewable Project Lifecycle</h4>
		</div>
	</div>
</section>