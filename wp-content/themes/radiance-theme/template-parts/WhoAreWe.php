<?php $whoweare = get_field('who_are_we'); ?>

<section class="Section WhoWeAre">
	<div class="container">
		<div class="TopHeading" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="200">
			<h2 class="OrangeBorderBottom GreyText"><?php echo $whoweare['heading']; ?></h2>
			<?php echo $whoweare['content']; ?>
			<a href="<?php echo $whoweare['link']; ?>" class="OrangeYellowButton"><span>Read More</span></a>
		</div>		
	</div>
</section>