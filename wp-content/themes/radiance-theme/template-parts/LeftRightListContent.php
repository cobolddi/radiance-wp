<?php 

$Leftrightlist = get_field('left_right_list_content');

?>


<section class="Section GradientBlock NewGradientBlock">
	<div class="container">
		<div class="TopWhiteContentOnly">
			<h2 class="WhiteText" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300"><?php echo $Leftrightlist['heading']; ?></h2>
			<div class="row">
				<div class="col-12 col-md-6" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
					<?php if( have_rows('left_right_list_content') ): while ( have_rows('left_right_list_content') ) : the_row(); ?>
						<ul>
							<?php if( have_rows('left_list_points') ): while ( have_rows('left_list_points') ) : the_row(); ?>
								<li><strong><?php echo get_sub_field('bold_head'); ?></strong> <?php echo get_sub_field('content'); ?></li>
							<?php endwhile; endif; ?>
						</ul>
					<?php endwhile; endif; ?>
				</div>
				<div class="col-12 col-md-6" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
					<?php if( have_rows('left_right_list_content') ): while ( have_rows('left_right_list_content') ) : the_row(); ?>
						<ul>
							<?php if( have_rows('right_list_ponits') ): while ( have_rows('right_list_ponits') ) : the_row(); ?>
								<li><strong><?php echo get_sub_field('bold_head'); ?></strong> <?php echo get_sub_field('content'); ?></li>
							<?php endwhile; endif; ?>
						</ul>
					<?php endwhile; endif; ?>
				</div>
			</div>
		</div>
		<div class="ContentWithLink">
			<?php echo $Leftrightlist['bottom_big_text']; ?>
			
		</div>
	</div>
</section>