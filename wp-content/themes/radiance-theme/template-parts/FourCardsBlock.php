<?php 

$fourcardsblock = get_field('four_cards_block');

?>


<section class="Section LiteOrangeSection FourCardBlock">
	<div class="container">
		<div class="TopHeading SmallContainer" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="200">
			<h2 class="LiteOrangeBorderBottom"><?php echo $fourcardsblock['heading']; ?></h2>
			<p><?php echo $fourcardsblock['subheading']; ?></p>
		</div>
		<div class="FourCardWithLink">
			<?php if( have_rows('four_cards_block') ): while ( have_rows('four_cards_block') ) : the_row(); ?>
				<div class="row">
					<?php if( have_rows('cards') ): while ( have_rows('cards') ) : the_row(); ?>
						<div class="col-6 col-md-3" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
							<a href="<?php echo get_sub_field('card_link'); ?>">
								<?php echo get_sub_field('card_text'); ?>
								<span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arw-right.svg" alt=""></span>
							</a>
						</div>
					<?php endwhile; endif; ?>
				</div>
			<?php endwhile; endif; ?>
		</div>
	</div>
</section>