<section class="HomeBanner">
	<?php if( have_rows('home_banner') ): while ( have_rows('home_banner') ) : the_row(); ?>	
		<div class="Homeslider">
			<?php if( have_rows('banner_slider') ): while ( have_rows('banner_slider') ) : the_row(); ?>
			  	<div class="Cards">
					<picture>
						<source media="(min-width:768px)" srcset="<?php echo get_sub_field('desktop_banner'); ?>" alt="">
						<img src="<?php echo get_sub_field('mobile_banner'); ?>" alt="">
					</picture>
					
					<div class="BannerContent">
						<div class="container">
							<div class="BannerText">
								<?php if(get_sub_field('logo')){ ?>
									<img src="<?php echo get_sub_field('logo'); ?>" alt="" class="radiancelogo">
								<?php } ?>
								<?php if(get_sub_field('top_heading')){ ?>
									<h4><?php echo get_sub_field('top_heading'); ?></h4>
								<?php } ?>
									<h1 class="OrangeBorderBottom"><?php echo get_sub_field('bottom_heading'); ?></h1>
								<?php if(get_sub_field('link_text')){ ?>
									<a href="<?php echo get_sub_field('link'); ?>" class="OrangeYellowButton"><span><?php echo get_sub_field('link_text'); ?></span></a>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			<?php endwhile; endif; ?>
		</div>
	<?php endwhile; endif; ?>
</section>





