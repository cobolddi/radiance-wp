<?php $radiancebanner = get_field('the_radiance_banner'); ?>


<section class="HomeBanner InsideBanner PortfolioBanner InsideBigBanner">
	<picture>
		<source media="(min-width:465px)" srcset="<?php echo $radiancebanner['desktop_image']['url']; ?>">
		<img src="<?php echo $radiancebanner['mobile_image']['url']; ?>" alt="Radiance Renewable">
	</picture>
	<div class="BannerContent">
		<div class="container">
			<div class="BannerText" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="700">
				<h1 class="OrangeBorderBottom"><?php echo $radiancebanner['page_title']; ?></h1>
			</div>
		</div>
	</div>
	<div class="RadianceBanner" id="whyradiance">
		
	</div>
</section>