<?php $insidebanner = get_field('inside_page_banner'); ?>

<section class="HomeBanner InsideBanner">
	<picture>
		<source media="(min-width:640px)" srcset="<?php echo $insidebanner['desktop_image']['url']; ?>">
		<img src="<?php echo $insidebanner['mobile_image']['url']; ?>" alt="Radiance Renewable">
	</picture>
	<div class="BannerContent">
		<div class="container">
			<div class="BannerText" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="700">
				<h1 class="OrangeBorderBottom"><?php echo $insidebanner['page_title']; ?></h1>
			</div>
		</div>
	</div>
</section>