<?php $portfoliobanner = get_field('portfolio_banner'); ?>

<section class="HomeBanner InsideBanner PortfolioBanner InsideBigBanner">
	<picture>
		<source media="(min-width:465px)" srcset="<?php echo $portfoliobanner['desktop_image']['url']; ?>">
		<img src="<?php echo $portfoliobanner['mobile_image']['url']; ?>" alt="Radiance Renewable">
	</picture>
	<div class="BannerContent">
		<div class="container">
			<div class="BannerText" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="700">
				<h1 class="OrangeBorderBottom"><?php echo $portfoliobanner['page_title']; ?></h1>
			</div>
		</div>
	</div>
</section>