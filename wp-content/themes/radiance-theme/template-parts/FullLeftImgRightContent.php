<?php 

$fullLeftImgRightContent = get_field('full_left_img_right_content'); 

?>


<section class="left_image_right_content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 IpadView" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
              <div class="imgWrap">
                  <img src="<?php echo $fullLeftImgRightContent['left_img']['url']; ?>" alt="gift_image">
              </div>
            </div>
            <div class="col-md-6 IpadView">
                <div class="contentWrap mr-5" data-aos="fade-out" data-aos-easing="linear" data-aos-duration="550">
                    <h2 class="LiteOrangeBorderBottom GreyText"><?php echo $fullLeftImgRightContent['heading']; ?></h2>
                    <?php echo $fullLeftImgRightContent['content']; ?>
                    
                </div>
            </div>            
        </div>
    </div>
</section>