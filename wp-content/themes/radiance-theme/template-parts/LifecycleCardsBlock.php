<section class="Section LifeCycleThreeCardsBlock">
	<div class="container">
		<div class="TopImage DesktopOnly">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/lifecycle.png" alt="" class="lifecycle DesktopOnly" data-aos="fade-out" data-aos-easing="linear" data-aos-duration="200">
		</div>
		<div class="Lifecycle MobileOnly">
			<!-- <div class="Toparw">
				<h4>Land. Evacuation & Approvals</h4>
				<img src="assets/img/top-arrow.png" alt="">
			</div> -->
			<div class="LifecycleSlider">
			  	<div class="Cards">
					<div class="topBlock">
						<p>Project<br> Evaluation</p>
						<p>Customised<br> Design</p>
					</div>
					<div class="bottomBlock">
						<p class="leftbox">Understand <br>sustainability<br> and customer <br>needs</p>
						<p class="rightbox">Conceptualise <br>long-term<br> renewable energy<br> solution</p>
					</div>
				</div>
				<div class="Cards lifecycle">
					<div class="topBlock">
						<p>Engineering &<br> procurement</p>
						<p>Construction &<br> Commissioning</p>
					</div>
					<div class="bottomBlock">
						<p class="leftbox">Best in class<br> components and<br> expert led site<br> planning</p>
						<p class="rightbox">High- quality<br> installation and<br> seamless<br> connection</p>
					</div>
				</div>
				<div class="Cards">
					<div class="topBlock">
						<p>Operations &<br> Maintenance</p>
						<p>Asset <br> Management</p>
					</div>
					<div class="bottomBlock">
						<p class="leftbox">Optimizing <br>Performance and <br>insuring long term/ <br>lifecycle efficiency <br>of installation</p>
						<p class="rightbox">Focus on output, <br>learning, effective <br>management and <br>corporate goverance</p>
					</div>
				</div>
			</div>
			<!-- <div class="BottomArw">
				<img src="assets/img/bottom-arrow.png" alt="">
				<h4>Financing</h4>
			</div> -->
		</div>
		<div class="ThreeCardsBlock">
			<div class="row">
				<div class="col-12 col-md-4">
					<div class="CardsBlock">
						<div class="LeftIconRightContent" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
							<div class="icon">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/phase1.svg" alt="">
							</div>
							<h4>Phase 1:<br><span>Evaluating, Design Thinking & Planning</span></h4>
						</div>
						<div class="BtmBoxBorder" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
							<p><strong>Outcome</strong>: Recommending the correct and customised renewables solution for our customers</p>
							<ul>
								<li>Studying the requirements of our customer</li>
								<li>Site visits and inspection by our technical team</li>
								<li>Designing of the most appropriate system taking into consideration the technical, commercial & regulatory requirements and constraints</li>
								<li>Site orientation and shadow effects</li>
								<li>Power evacuation modes</li>
								<li>Fire safety and security requirements in line with standards</li>

							</ul>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="CardsBlock">
						<div class="LeftIconRightContent" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="350">
							<div class="icon">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/phase2.svg" alt="">
							</div>
							<h4>Phase 2:<br><span>Engineering & Installing</span></h4>
						</div>
						<div class="BtmBoxBorder" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="350">
							<p><strong>Outcome</strong>: Setting up the renewable solutions on or off site</p>
							<ul>
								<li>Engineering, procuring, installing, integrating and commissioning of the solar power system at the customer site or at an offsite location for open access projects</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="CardsBlock">
						<div class="LeftIconRightContent" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
							<div class="icon">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/phase3.svg" alt="">
							</div>
							<h4>Phase 3:<br><span>Proactive Asset Management</span></h4>
						</div>
						<div class="BtmBoxBorder" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
							<p><strong>Outcome</strong>: Operating and maintaining the renewable energy plant in a smooth, efficient and best in class manner with zero headache for the customer</p>
							<ul>
								<li>Proactive Asset Management of the renewable energy assets over the contracted period</li>
								<li>OPEX optimization with increased efficiency & energy delivery</li>
								<li>Optimal Operations with an aim to maximize production with minimized cost</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>