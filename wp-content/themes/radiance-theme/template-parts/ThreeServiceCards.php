<section class="Section LiteOrangeSection ThreeServiceCardsBlock">
	<div class="container">
		<div class="TopHeading SmallContainer" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="200">
			<h2 class="LiteOrangeBorderBottom">Futuristic Solutions</h2>
			<p>Radiance Renewables also offers services in the following areas:</p>
		</div>
		<div class="ThreeServiceCards">
			<div class="row">
				<div class="col-12 col-md-4" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
					<div class="IconCards">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/solarbattery.svg" alt="">
						<h4>Solar and Battery Storage/Hydrogen<br> Hybrid Solutions</h4>
						<ul>
							<li>Commercial and Industrial Greenfield Development and Operational Infrastructure Assets/Platforms</li>
							<li>Customized Solutions as per client’s requirements</li>
							<li>Proven and recognized technology solutions and services</li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-md-4" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
					<div class="IconCards">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/windsolar.svg" alt="">
						<h4>Wind and Solar Hybrid Solar Solutions</h4>
						<ul>
							<li>Commercial and Industrial Greenfield Development and Operational Infrastructure Assets/Platforms</li>
							<li>Supply & Integrating Hybrid power for lower Levelized Cost of Electricity</li>
							<li>Personalised turnkey projects, Engineering, Procurement and Construction scope and Operations and Maintenance</li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-md-4" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="500">
					<div class="IconCards">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/geberation.svg" alt="">
						<h4>Other forms of Generation</h4>
						<ul>
							<li>Hydro/Geothermal</li>
							<li>Choosing best in class energy source</li>
							<li>Constructive use of waste for reducing CO2 emissions</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>