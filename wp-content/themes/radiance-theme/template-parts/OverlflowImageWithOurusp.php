<?php 

$OurUsp = get_field('overflow_image_with_usps');

?>


<section class="OverlflowImageWithOurusp" id="USPcards">
	<div class="container">
		<div class="TopImage" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="450">
			<picture>
				<source media="(min-width:640px)" srcset="<?php echo $OurUsp['overflow_desktop_image']['url']; ?>">
				<img src="<?php echo $OurUsp['overflow_mobile_image']['url']; ?>" alt="Radiance Renewable">
			</picture>
		</div>
		<div class="UspCardsWithHeading">
			<div class="SmallContainer">
				<div class="CenterHeading">
					<h2 class="LiteOrangeBorderBottom"><?php echo $OurUsp['usp_heading']; ?></h2>
					<p><?php echo $OurUsp['usp_content']; ?></p>
				</div>
			</div>	
			<div class="UspCardsBlock">
				<?php if( have_rows('overflow_image_with_usps') ): while ( have_rows('overflow_image_with_usps') ) : the_row(); ?>
					<div class="row">
						<?php if( have_rows('usp_cards') ): while ( have_rows('usp_cards') ) : the_row(); ?>
							<div class="col-12 col-md-6" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
								<div class="Cards">
									<div class="TopOrangeHeading" >
										<h4>
											<img src="<?php echo get_sub_field('icon'); ?>" alt="">
											<span><?php echo get_sub_field('heading'); ?></span>
										</h4>
									</div>
									<div class="BottomWhiteContent">
										<?php echo get_sub_field('bottom_content'); ?>
									</div>
								</div>
							</div>
						<?php endwhile; endif; ?>
					</div>
				<?php endwhile; endif; ?>
				<div class="row">
					<div class="col-12 col-md-12" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
						<div class="Cards">
							<div class="TopOrangeHeading" >
								<h4>
									<img src="<?php echo $OurUsp['unique_lineage_icon']['url']; ?>" alt="">
									<span><?php echo $OurUsp['unique_lineage_heading']; ?></span>
								</h4>
							</div>
							<div class="BottomWhiteContent IconBlock">
								<p><?php echo $OurUsp['unique_lineage_content']; ?></p>
								<?php if( have_rows('overflow_image_with_usps') ): while ( have_rows('overflow_image_with_usps') ) : the_row(); ?>
									<ul>
										<?php if( have_rows('unique_lineage_cards') ): while ( have_rows('unique_lineage_cards') ) : the_row(); ?>
											<li>
												<img src="<?php echo get_sub_field('icon'); ?>" alt="">
												<p><?php echo get_sub_field('heading'); ?></p>
											</li>
										<?php endwhile; endif; ?>
									</ul>
								<?php endwhile; endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="AllLineage" id="RadianceProcess">
				<a href="<?php echo $OurUsp['lineage_link']; ?>">Click here to read more about Radiance’s Lineage.</a>
			</div>
		</div>
	</div>
</section>