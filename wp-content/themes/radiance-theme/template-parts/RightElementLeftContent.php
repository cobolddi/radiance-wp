<?php $whoarewe = get_field('who_are_we'); ?>

<section class="RightElementLeftContent">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
				<div class="LeftContentBlock">
					<h2 class="OrangeBorderBottom GreyText"><?php echo $whoarewe['heading']; ?></h2>
					<?php echo $whoarewe['content']; ?>
				</div>
			</div>
			<div class="col-12 col-md-4" data-aos="fade-out" data-aos-easing="linear" data-aos-duration="400">
				<div class="RightImg">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/pinkelement.svg" alt="">
				</div>
			</div>
		</div>
	</div>
</section>