
</main>
<footer>
	<div class="container">
		<div class="TopFooterBlock">
			<div class="LogoGrid">
				<a href="index.php" class="logo">
					<img src="assets/img/logo.svg" alt="">
				</a>
			</div>
			<div class="NewsletterGrid MobileOnly">
				<div class="Grids">
					<h6>Get The Newsletter</h6>
					<form action="">
						<input type="email" placeholder="Email">
						<input type="submit" value="">
					</form>
				</div>
			</div>
			<div class="LinkGrid MobileOnly">
				<div class="Grids">
					<h6>Quick Links</h6>
					<ul>
						<li><a href="who-we-are.php">About</a></li>
						<li><a href="project-faq.php">Project FAQs</a></li>
						<li><a href="team.php">Our Team</a></li>
						<li><a href="portfolio.php">Portfolio</a></li>
						<li><a href="the-radiance-way.php">The Radiance Way</a></li>
						<li><a href="contact-us.php">Contact us</a></li>
						<li><a href="solutions.php">Solutions</a></li>
						<li><a href="work-with-us.php">Work with us</a></li>
					</ul>
				</div>
			</div>
			<div class="EmailGrid">
				<div class="Grids">
					<h6>One Indiabulls Centre</h6>
					<p>16th floor Tower 2A <br>Senapati Bapat Marg <br>Elphinstone Road, Mumbai - 400013 </p>
					<p>Email: <a href="mailto:contact@radiancerenewables.com" target="_blank">contact@radiancerenewables.com</a></p>
					<a href="https://www.linkedin.com/company/radiance-renewables" target="_blank"><img src="assets/img/linkedin-logo.svg" alt=""></a>
				</div>
			</div>
			<div class="LinkGrid DesktopOnly">
				<div class="Grids">
					<h6>Quick Links</h6>
					<ul>
						<li><a href="who-we-are.php">About</a></li>
						<li><a href="project-faq.php">Project FAQs</a></li>
						<li><a href="team.php">Our Team</a></li>
						<li><a href="portfolio.php">Portfolio</a></li>
						<li><a href="the-radiance-way.php">The Radiance Way</a></li>
						<li><a href="contact-us.php">Contact us</a></li>
						<li><a href="solutions.php">Solutions</a></li>
						<li><a href="work-with-us.php">Work with us</a></li>
					</ul>
				</div>
			</div>
			<div class="NewsletterGrid DesktopOnly">
				<div class="Grids">
					<h6>Get The Newsletter</h6>
					<form action="">
						<input type="email" placeholder="Email">
						<input type="submit" value="">
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="BottomFooter">
		<div class="container">
			<p>All rights reserved. © 2020 Radiance Renewables</p>
		</div>
	</div>
</footer>

<script src="assets/js/vendor.min.js"></script>
<script src="assets/js/scripts.min.js"></script>
</body>
</html>