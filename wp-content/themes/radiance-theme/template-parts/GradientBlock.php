<?php $gradientBlock = get_field('the_radiance_milestone'); ?>

<section class="Section GradientBlock">
	<div class="container">
		<div class="HeadingWithIconBox">
			<h2 class="WhiteText WhiteBorderBottom" data-aos="fade-in" data-aos-duration="100"><?php echo $gradientBlock['heading']; ?></h2>
			<?php if( have_rows('the_radiance_milestone') ): while ( have_rows('the_radiance_milestone') ) : the_row(); ?>
				<div class="row">
					<?php if( have_rows('icon_with_heading') ): while ( have_rows('icon_with_heading') ) : the_row(); ?>
						<div class="col-6 col-md-3" data-aos="fade-out" data-aos-duration="<?php echo get_sub_field('animation_timer'); ?>">
							<div class="IconWithHeading">
								<div class="Icons">
									<img src="<?php echo get_sub_field('icon'); ?>" alt="">
								</div>
								<h4><?php echo get_sub_field('heading'); ?></h4>
								<p><?php echo get_sub_field('subheading'); ?></p>
							</div>
						</div>
					<?php endwhile; endif; ?>
				</div>
			<?php endwhile; endif; ?>
		</div>
		<!-- <div class="LearnMore" data-aos="fade-out" data-aos-duration="500">
			<a href="#" class="WhiteButton"><span>Learn More</span></a>
		</div> -->
	</div>
</section>