<?php 

$IconContent = get_field('image_with_iconheading'); 
$Topimg = $IconContent['top_desktop_image'];
$Topimgmobile = $IconContent['top_mobile_image'];


?>


<section class="OverlflowImageWithIconBox">
	<div class="container">
		<div class="TopImage" data-aos="fade-out" data-aos-easing="linear" data-aos-duration="100">
			<picture>
				<source media="(min-width:640px)" srcset="<?php echo $Topimg; ?>">
				<img src="<?php echo $Topimgmobile; ?>" alt="Radiance Renewable">
			</picture>
		</div>
		<div class="LeftIconsRightImage">
			<div class="IconBox">
				<h2 data-aos="fade-in" data-aos-easing="linear" data-aos-duration="450"><?php echo $IconContent['heading']; ?></h2>
				<h4 data-aos="fade-in" data-aos-easing="linear" data-aos-duration="480"><?php echo $IconContent['subheading']; ?></h4>
				<?php if( have_rows('image_with_iconheading') ): while ( have_rows('image_with_iconheading') ) : the_row(); ?>
					<div class="row">
						<?php if( have_rows('icon_with_content') ): while ( have_rows('icon_with_content') ) : the_row(); ?>
							<div class="col-6 col-md-4" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="120">
								<div class="IconWithHeading">
									<div class="Icon">
										<img src="<?php echo get_sub_field('icon'); ?>" alt="">
									</div>
									<h4><?php echo get_sub_field('icon_heading'); ?></h4>
									<p><span><?php echo get_sub_field('icon_subheading'); ?></span><br> <?php echo get_sub_field('icon_bottom_content'); ?></p>
								</div>
							</div>
						<?php endwhile; endif; ?>
					</div>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</div>
</section>