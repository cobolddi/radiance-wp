<?php 

$containerleftright = get_field('container_left_logo_right_content'); 

?>

<section class="Section GreyBgSection ContainerLeftImgRightContent" id="lineage">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="imgWrap">
                    <img src="<?php echo $containerleftright['top_logo']['url']; ?>" alt="">
                    <div class="logobox">
                        <img src="<?php echo $containerleftright['bottom_left_logo']['url']; ?>" alt="">
                        <img src="<?php echo $containerleftright['bottom_right_logo']['url']; ?>" alt="">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="contentWrap">
                    <h2 class="LiteOrangeBorderBottom GreyText"><?php echo $containerleftright['heading']; ?></h2>          
                    <?php echo $containerleftright['content']; ?>
                </div>
            </div>
        </div>
    </div>
</section>