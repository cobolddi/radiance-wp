<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

</main>
<footer>
	<div class="container">
		<div class="TopFooterBlock">
			<div class="LogoGrid">
				<a href="<?php bloginfo( 'url' ); ?>" class="logo">
					<img src="<?php echo get_field('footer_logo', 'option')['url']; ?>" alt="">
				</a>
			</div>
			<div class="NewsletterGrid MobileOnly">
				<div class="Grids">
					<h6>Get The Newsletter</h6>
					<?php echo do_shortcode('[contact-form-7 id="525" title="Newsletter form"]');?>
					
				</div>
			</div>
			<div class="LinkGrid MobileOnly">
				<div class="Grids">
					<h6>Quick Links</h6>
					<?php
						wp_nav_menu(
							array(
								'theme_location' => 'footer-menu',
								'menu_id'        => 'footer-menu',
							)
						);
					?>
				</div>
			</div>
			<div class="EmailGrid">
				<div class="Grids">
					<?php the_field('address', 'option'); ?>					
					<p>Email: <a href="mailto:<?php the_field('email_address', 'option'); ?>" target="_blank"><?php the_field('email_address', 'option'); ?></a></p>
					<a href="<?php the_field('linkedin_link', 'option'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/linkedin-logo.svg" alt=""></a>
				</div>
			</div>
			<div class="LinkGrid DesktopOnly">
				<div class="Grids">
					<h6>Quick Links</h6>
					<?php
						wp_nav_menu(
							array(
								'theme_location' => 'footer-menu',
								'menu_id'        => 'footer-menu',
							)
						);
					?>
				</div>
			</div>
			<div class="NewsletterGrid DesktopOnly">
				<div class="Grids">
					<h6>Get The Newsletter</h6>
					<?php echo do_shortcode('[contact-form-7 id="525" title="Newsletter form"]');?>
					<div id="small-dialog" class="zoom-anim-dialog mfp-hide">
						<h2>Thank you!</h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="BottomFooter">
		<div class="container">
			<p>All rights reserved. © 2021 Radiance Renewables</p>
		</div>
	</div>
</footer>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
