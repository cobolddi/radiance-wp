<?php
/**
 * Template Name: About Us Page Template
 * Author: Rahul Ranjan
 * Email: rahul@cobold.in
 * @package _s
 */

get_header();

?>

<?php @include('template-parts/RightElementLeftContent.php') ?>

<?php @include('template-parts/TopIconBottomContent.php') ?>

<?php @include('template-parts/containerLeftRightContent.php') ?>

<?php @include('template-parts/FullLeftImgRightContent.php') ?>

<?php @include('template-parts/FullRightImgLeftContent.php') ?>
	


<?php
get_footer();
?>