<?php
/**
 * Template Name: Portfolio Page Template
 * Author: Rahul Ranjan
 * Email: rahul@cobold.in
 * @package _s
 */

get_header();

?>

<?php 

$article = get_field('article_group');

?>

<section class="Section ArticlesSection">
	<div class="container">
		<div class="TopHeading">
			<h2 class="LiteOrangeBorderBottom"><?php echo $article['heading']; ?></h2>
		</div>
		<div class="ArticlesBlock">
			<?php if( have_rows('article_group') ): while ( have_rows('article_group') ) : the_row(); ?>	
				<div class="Articlesslider">
					<?php if( have_rows('article_slider') ): while ( have_rows('article_slider') ) : the_row(); ?>
					  	<div class="Cards">
							<div class="TopImg">
								<img src="<?php echo get_sub_field('article_image'); ?>" alt="">
							</div>
							<div class="BottomContent">
								<h4><?php echo get_sub_field('article_heading'); ?></h4>
								<a href="<?php echo get_sub_field('article_link'); ?>" class="OrangeYellowButton" target="_blank"><span>Read more</span></a>
							</div>
						</div>
					<?php endwhile; endif; ?>
				</div>
			<?php endwhile; endif; ?>
		</div>
	</div>
</section>

<section class="Section LiteOrangeSection ProjectsPipelineTables">
	<div class="container">
		<div class="TopHeading">
			<h2 class="LiteOrangeBorderBottom">Existing Projects & Pipeline</h2>
		</div>
		<div class="PipelineProjects">
			<div class="row">
				<div class="col-12 col-md-12">
					<div class="Cards">
						<div class="TopImgBlock">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/tempimg/projectcard.png" alt="">
						</div>
						<div class="TopOrangeHeading">
							<h4 class="FullwidthHeading">Open Access Project</h4>
							<ul class="EnergyState">
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/power.svg" alt="">
									<span>21.00 MWp</span>
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/state.svg" alt="">
									<span>Karnataka</span>
								</li>
							</ul>
						</div>
						<div class="BottomWhiteContent Fullwidthcontent">
							<h5>Project Off-takers</h5>
							<ul class="CircleBullet HalfWidthContent">
								<li>Large Japanese auto manufacturer</li>
								<li>Leading exporter of FIBC products</li>
								<li>Leading manufacturer and supplier of precision engineered products</li>
								<li>Leading Indian manufacturer of valve seat inserts and turbo charger parts for IC engines</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<?php if( have_rows('projects_card') ): while ( have_rows('projects_card') ) : the_row(); ?>
				<div class="row Pipelinecards">
					<?php if( have_rows('cards') ): while ( have_rows('cards') ) : the_row(); ?>
						<div class="col-12 col-md-4 Cardsforloadmore">
							<div class="Cards">
								<div class="TopImgBlock">
									<img src="<?php echo get_sub_field('top_image'); ?>" alt="">
								</div>
								<div class="TopOrangeHeading">
									<h4><?php echo get_sub_field('heading'); ?></h4>
									<ul class="EnergyState">
										<li>
											<img src="<?php echo get_template_directory_uri(); ?>/assets/img/power.svg" alt="">
											<span><?php echo get_sub_field('power'); ?></span>
										</li>
										<li>
											<img src="<?php echo get_template_directory_uri(); ?>/assets/img/state.svg" alt="">
											<span><?php echo get_sub_field('state'); ?></span>
										</li>
									</ul>
								</div>
								<div class="BottomWhiteContent">
									<?php echo get_sub_field('content'); ?>
								</div>
							</div>
						</div>
					<?php endwhile; endif; ?>
				</div>
			<?php endwhile; endif; ?>
		</div>
	</div>
</section>

<?php 

$csr = get_field('content_with_gallery');

?>

<section class="Section WhiteBgSection TopContentWithGallery">
	<div class="container">
		<div class="TopHeading">
			<h2 class="LiteOrangeBorderBottom"><?php echo $csr['top_heading']; ?></h2>
		</div>
		<div class="TopContentBox">
			<div class="row">
				<div class="col-12 col-md-6">
					<div class="Content">
						<p><?php echo $csr['top_left_content']; ?></p>
					</div>
				</div>
				<div class="col-12 col-md-6">
					<div class="Content">
						<p><?php echo $csr['top_right_content']; ?></p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-md-6">
					<div class="Content">
						<h2 class="LiteOrangeBorderBottom"><?php echo $csr['bottom_left_heading']; ?></h2>
						<p><?php echo $csr['bottom_left_content']; ?></p>
					</div>
				</div>
				<div class="col-12 col-md-6">
					<div class="Content">
						<img src="<?php echo $csr['bottom_right_image']['url']; ?>" alt="">
					</div>
				</div>
			</div>
		</div>
		<div class="GalleryBox">
			<?php if( have_rows('content_with_gallery') ): while ( have_rows('content_with_gallery') ) : the_row(); ?>
				<div class="row">
					<?php if( have_rows('gallery_box') ): while ( have_rows('gallery_box') ) : the_row(); ?>
						<div class="col-6 col-md-4">
							<div class="magnific-img">
								<a class="image-popup-vertical-fit" href="<?php echo get_sub_field('gallery_image'); ?>">
									<img src="<?php echo get_sub_field('gallery_image'); ?>" alt="" />
								</a>
							</div>
						</div>
					<?php endwhile; endif; ?>
				</div>
			<?php endwhile; endif; ?>
		</div>	
	</div>
</section>

<?php @include('template-parts/TestimonialsSlider.php') ?>


<?php
get_footer();
?>