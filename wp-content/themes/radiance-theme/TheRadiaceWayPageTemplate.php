<?php
/**
 * Template Name: Radiance Way Page Template
 * Author: Rahul Ranjan
 * Email: rahul@cobold.in
 * @package _s
 */

get_header();

?>

<?php @include('template-parts/CenterAlignContent.php') ?>

<?php @include('template-parts/LeftRightListContent.php') ?>

<?php @include('template-parts/OverlflowImageWithOurusp.php') ?>

<?php @include('template-parts/ContainerLeftImageRightContent.php') ?>


<?php
get_footer();
?>