<?php
/**
 * Template Name: Work With Us Page Template
 * Author: Rahul Ranjan
 * Email: rahul@cobold.in
 * @package _s
 */

get_header();

?>

<?php @include('template-parts/Recruitment.php') ?>

<?php @include('template-parts/Applynow.php') ?>

<?php
get_footer();
?>