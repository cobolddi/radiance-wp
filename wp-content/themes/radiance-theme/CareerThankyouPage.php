<?php
/**
 * Template Name: Career Thank you Page Template
 * Author: Rahul Ranjan
 * Email: rahul@cobold.in
 * @package _s
 */

get_header();

?>

<section class="Section ThankyouContentBlock">
	<div class="container">
		<div class="ThankyouContent">
			<div class="TickIcon">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/tick-icon.svg" alt="">
			</div>
			<h2 class="WhiteBorderBottom WhiteText">Thank you for<br>applying to Radiance Renewables!</h2>
			<p class="WhiteText">We appreciate your interest, time, and effort! The moment our recruitment team shortlists your CV or has further questions, we will contact you. In the meantime, stay in touch with us on our official <a href="https://www.linkedin.com/checkpoint/challengesV2/AQEzd-hxTpz0vAAAAXfOiPFUeYk45Czts_SpTVzoZi2fImC034C2tfi4zVMUX4c-FGTKF99UlqJMbp-Won0xYss_ZoFFbVAwqg?original_referer=http%3A%2F%2Flocalhost%3A3000%2Fradiance-ui%2Fwork-with-us.php" target="_blank">LinkedIn page!</a></p>
		</div>
	</div>
</section>


<?php
get_footer();
?>