<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/fav-icon.png">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">

<header id="header">
	<div class="container">
		<div class="row">
			<div class="col-6 col-md-3">
				<a href="<?php bloginfo( 'url' ); ?>" class="logo">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.svg" alt="">
				</a>
			</div>
			<div class="col-6 col-md-9">
				<div class="IpadDesktop">
					<nav>
						<?php
							wp_nav_menu(
								array(
									'theme_location' => 'menu-1',
									'menu_id'        => 'primary-menu',
								)
							);
						?>
					</nav>
				</div>
				<div class="IpadRemoved">
					<div class="MobileMenu">
						<button class="c-hamburger c-hamburger--htx">
					  		<span></span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>						
</header>


<nav class="Sub-menu open">
	<!-- <ul>
		<li>
			<a href="index.php">HOME</a>
		</li>
		<li class="haveSubmenu">
			<a href="who-we-are.php">ABOUT</a>
			<ul>
				<ul>
					<li>
						<a href="who-we-are.php">Who are we?</a>				
					</li>
					<li>
						<a href="team.php">Our Team</a>				
					</li>
				</ul>
			</ul>
		</li>
		<li class="haveSubmenu">
			<a href="the-radiance-way.php">THE RADIANCE WAY</a>
			<ul>
				<li>
					<a href="#whyradiance">Why Radiance?</a>				
				</li>
				<li>
					<a href="#USPcards">Our USPs</a>			
				</li>
				<li>
					<a href="#RadianceProcess">The Radiance Process</a>				
				</li>
			</ul>
		</li>
		<li>
			<a href="solutions.php">SOLUTIONS</a>
		</li>
		<li>
			<a href="portfolio.php">PORTFOLIO</a>
		</li>
		<li>
			<a href="project-faq.php">FAQs</a>
		</li>
		<li class="haveSubmenu">
			<a href="contact-us.php">CONTACT</a>
			<ul>
				<li>
					<a href="contact-us.php#FormBlock">Contact Form</a>				
				</li>
				<li>
					<a href="work-with-us.php">Careers</a>			
				</li>
			</ul>
		</li>
	</ul> -->
	<?php
		wp_nav_menu(
			array(
				'theme_location' => 'mobile-menu',
			)
		);
	?>
</nav>

<main>

<?php
	$term = get_queried_object();
	$headerType = get_field('header_type', $term);

	if($headerType == "homebanner") {
		get_template_part('template-parts/pageHeader/HomeBanner');
	} 
	elseif ($headerType == "insidebanner") {
		get_template_part('template-parts/pageHeader/InsideBanner');
	}
	elseif ($headerType == "portfoliobanner") {
		get_template_part('template-parts/pageHeader/PortfolioBanner');
	}
	elseif ($headerType == "radiancebanner") {
		get_template_part('template-parts/pageHeader/TheRadianceBanner');
	}
	else {
		get_template_part('ui-parts/template-parts/pageHeader/NoBanner');
	}

	 
?>