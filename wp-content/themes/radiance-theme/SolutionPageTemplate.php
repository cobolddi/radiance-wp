<?php
/**
 * Template Name: Solutions Page Template
 * Author: Rahul Ranjan
 * Email: rahul@cobold.in
 * @package _s
 */

get_header();

?>


<?php @include('template-parts/FourCardsBlock.php') ?>

<?php @include('template-parts/DarkBgSectionWithHeading.php') ?>

<?php @include('template-parts/LifecycleCardsBlock.php') ?>

<?php @include('template-parts/ThreeServiceCards.php') ?>







<?php
get_footer();
?>