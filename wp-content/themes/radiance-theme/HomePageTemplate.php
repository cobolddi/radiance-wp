<?php
/**
 * Template Name: Home Page Template
 * Author: Rahul Ranjan
 * Email: rahul@cobold.in
 * @package _s
 */

get_header();

?>



<?php //@include('template-parts/pageHeader/HomeBanner.php') ?>

<?php @include('template-parts/WhoAreWe.php') ?>

<?php //@include('template-parts/LeftImageRightContent.php') ?>

<?php @include('template-parts/GradientBlock.php') ?>

<?php @include('template-parts/OverlflowImageWithIconBox.php') ?>

<?php
get_footer();
?>