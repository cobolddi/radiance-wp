<?php
/**
 * Template Name: Contact Page Template
 * Author: Rahul Ranjan
 * Email: rahul@cobold.in
 * @package _s
 */

get_header();

?>

<?php 

$formDetails = get_field('form_details'); 

?>

<section class="ContactUsSection">
	<div class="container">
		<div class="ContactAddress">
			<div class="FormBlock" id="FormBlock" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="350">
				<h2 class="LiteOrangeBorderBottom GreyText"><?php echo $formDetails['heading']; ?></h2>
				<?php echo $formDetails['content']; ?>
				
				<?php echo do_shortcode('[contact-form-7 id="161" title="Contact Page Form"]'); ?>
				
			</div>
			<div class="AddressBlock" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="350">
				<?php the_field('contact_page_add', 'option'); ?>
				<p class="phone">Phone: <a href="tel:<?php the_field('phone_no', 'option'); ?>"><?php the_field('phone_no', 'option'); ?></a></p>
				<p>Email: <a href="mailto:<?php the_field('email_address', 'option'); ?>"><?php the_field('email_address', 'option'); ?></a></p>
			</div>
		</div>
	</div>										
</section>

<?php 

$iframe = get_field('map_address'); 

?>

<section class="GoogleMapSection">
	<iframe src="<?php echo $iframe['iframe_link']; ?>" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</section>

<?php
get_footer();
?>