<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package _s
 */

get_header();
?>

	

<?php $singlenlogbanner = get_field('single_blog_banner'); ?>

<section class="HomeBanner InsideBanner">
	<picture>
		<source media="(min-width:640px)" srcset="<?php echo $singlenlogbanner['desktop_image']['url']; ?>">
		<img src="<?php echo $singlenlogbanner['mobile_image']['url']; ?>" alt="Radiance Renewable">
	</picture>
</section>


<section class="Section OverlapContentBlock">
	<div class="container">
		<div class="BlogContent">
			<h1><?php the_title(); ?></h1>
			<ul class="authordate">
				<li><span>By:</span> <?php the_field("author_name"); ?></li>
				<li><?php echo get_the_date(); ?></li>
			</ul>
		</div>
	</div>
</section>

<section class="Section SinglePostContent">
	<div class="container">
		
		<div class="ContentBlock">
			<div class="SocialIcons">
				<ul>
					<li><a href="<?php the_field('linkedin_link', 'option'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/linkedin.svg" alt=""></a></li>
				</ul>
			</div>
			<h4><?php the_field("singleblog_subheading"); ?></h4>
			<?php echo the_content(); ?>
		</div>
	</div>
</section>


<?php
get_footer();