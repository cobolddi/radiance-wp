<?php
/**
 * Template Name: Our Team Page Template
 * Author: Rahul Ranjan
 * Email: rahul@cobold.in
 * @package _s
 */

get_header();

?>


<?php @include('template-parts/TeamCardsBlock.php') ?>


<?php
get_footer();
?>